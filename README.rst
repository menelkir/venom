Menelkir Venom Overlay
======================

This is a overlay with a bunch of software not available in official repositories 
(also serving as testing repository).

You can check the `TODO <https://gitlab.com/menelkir/venom/-/blob/main/TODO.rst>`_ to see what I'm going to do next.

=================================
How to Contribute to this Overlay
=================================

:author: Daniel Menelkir
:contact: menelkir@itroll.org
:language: English

==================
How to report bugs
==================

This overlay isn't connected in any way to VenomLinux Repositories.
If you are using this repository, please report any issues direct to me or
using the gitlab issue tracker.

=============
How to donate
=============

If you find this repo useful (don't forget to pay a visit to the related
repos too), you can buy me a beer:

:BTC: 3ECzX5UhcFSRv6gBBYLNBc7zGP9UA5Ppmn

:ETH: 0x7E17Ac09Fa7e6F80284a75577B5c1cbaAD566C1c
