#!/bin/sh

if [ -f /lib/modules/KERNELVERSION ]; then
	kernver=$(cat /lib/modules/KERNELVERSION)
else
	kernver=$(file /boot/vmlinuz-zen-venom  | cut -d ' ' -f9)
fi
if [ $(command -v mkinitramfs) ]; then
	mkinitramfs -q -k $kernver -o /boot/initrd-zen-venom.img
fi
depmod $kernver
