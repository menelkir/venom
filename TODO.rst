DONE:
-----

* g15stats

----------------
DONE with errors
----------------

None yet.

TODO: 
-----

------------
Top Priority
------------
* megasync
* veracrypt
* linux-zen

---------------
Medium Priority
---------------
* nicotine+
* strawberry

------------
Low Priority
------------
* calibre
* element-desktop-bin
* fuse-emulator
* gzdoom
* i3status-rust
* kitty
* openmsx
* openmsx-catapult
* qtwebflix
* rclone
* stremio
* w3m-imgcat
* wine-staging
